﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace examen_final
{
    class Program
    {
        static void Main(string[] args)
        {
            //Variables TODO: Organizar mejor
            int Opcion = 0;
            string Contra = "";
            string NombreUsuario = "";
            string ApellidoUsuario = "";
            string ContraUsuario = "";
            string CorreoUsuario = "";
            string CONNETION_STRING = "Data Source=localhost;Database=ExamenFinal;User ID=sa;Password=Only2open2c@";
            SqlConnection conexion = new SqlConnection(CONNETION_STRING);
            string query = "";
            int id = 0;
            SqlCommand comando = null;
            SqlDataReader lector = null;
            int idSeleccionado = 0;
            string IniciarCorreo = "";
            string IniciarContra = "";
            string CorreoDB = "";
            string ContraDB = "";
            int idDB = 0;
            int Opcion2 = 0;
            int idParaEnviar = 0;
            string AsuntoCorreo = "";
            string MensajeCorreo = "";
            int LeerCorreo = 0;


            //Funcion para crear usuario
            static void InputAgregarUsuario(string contra, string nombreUsuario, string apellidoUsuario, string contraUsuario, string correoUsuario, SqlConnection conexion, string query, int id, SqlCommand comando)
            {

                Console.Clear();
                Console.Write("Por favor indique la Contraseña del usuario administrador: ");
                contra = OcultarContra();
                //Console.Clear();

                //Validar la Contraseña dentro del sistema para poder crear un usuario.
                if (contra == "Abcd1234")
                {

                    //Inicio de la solicitu de datos del usuario
                    Console.WriteLine("A continuacion se van a solicitar los datos del usuario.");

                    Console.Write("\nIndique el primer Nombre del usuario: ");
                    nombreUsuario = VerificacionString().ToLower();

                    Console.Write("\nIndique el primer Apellido del usuario: ");
                    apellidoUsuario = VerificacionString().ToLower();


                    Console.Write("\nIndique la Contraseña del usuario: ");
                    contraUsuario = OcultarContra();

                    correoUsuario = $"{nombreUsuario}.{apellidoUsuario}@grupo7.com"; //interpolar para que el Correo sea el Nombre y el Apellido del usuario agregando grupo7 como dominio de Correo.

                    Console.WriteLine($"\nEl Correo del usuario es: {correoUsuario}");
                    Console.ReadKey();
                    //fin de la solicitud de datos del usaurio,(la funcion VerificacionString es para evitar que el usuario deje los campos vacios)

                    //TODO: Convertir funcion ingresar usuario para que interprete objetos tipo usuarios
                    //Inicio del metodo para ingresar los datos del usurio a la base de datos
                    IngresarUsuario(query, nombreUsuario, apellidoUsuario, correoUsuario, contraUsuario, comando, conexion, id);

                }
                else //Caso en que se ingrese una Contraseña incorrecta.
                {
                    Console.WriteLine("Contraseña Incorrecta.");
                    Console.ReadKey();
                    //Console.Clear();
                }


            }

            //****************Funcion para modificar Usuarios*******************


            static void InputModificarUsuario(string contra, string nombreUsuario, string apellidoUsuario, string contraUsuario, string correoUsuario, SqlConnection conexion, string query, SqlCommand comando, SqlDataReader lector, int idSeleccionado)
            {
                //Opcion 2 para cuando sea requerido modificar algun usuario en espeficio
                Console.Clear();
                Console.Write("Por favor indique la Contraseña del usuario administrador: ");
                contra = OcultarContra();
                Console.Clear();

                //Validar la Contraseña dentro del sistema para poder crear un usuario.
                if (contra == "Abcd1234")
                {
                    UsariosDisponibles(query, comando, conexion, lector); //Metodo para imprimir los usuarios disponibles.

                    Console.Write("\nIndique el ID del usuario que el desea modificar: ");

                    try
                    {
                        idSeleccionado = Convert.ToInt32(Console.ReadLine());
                    }
                    catch (Exception e)
                    {
                        Console.Clear();
                        Console.WriteLine($"Se produjo un error de tipo: {e.GetType().ToString()}");
                        Console.ReadKey();
                        Console.Clear();
                        return;
                    }

                    Console.Clear();
                    //Inicio de la solicitu de datos del usuario
                    Console.WriteLine("A continuacion se van a solicitar los datos del usuario.");

                    Console.Write("\nIndique el primer Nombre del usuario: ");
                    nombreUsuario = VerificacionString().ToLower();

                    Console.Write("\nIndique el primer Apellido del usuario: ");
                    apellidoUsuario = VerificacionString().ToLower();

                    Console.Write("\nIndique la Contraseña del usuario: ");
                    contraUsuario = OcultarContra();

                    correoUsuario = $"{nombreUsuario}.{apellidoUsuario}@grupo7.com"; //interpolar para que el Correo sea el Nombre y el Apellido del usuario agregando grupo7 como dominio de Correo.

                    Console.WriteLine($"\nEl Correo del usuario es: {correoUsuario}");
                    Console.ReadKey();
                    //fin de la solicitud de datos del usaurio

                    //Inicio del metodo para modificar los datos del usurio a la base de datos
                    ModificarUsario(query, nombreUsuario, apellidoUsuario, correoUsuario, contraUsuario, idSeleccionado, comando, conexion);

                }
                else //Caso en que se ingrese una Contraseña incorrecta.
                {
                    Console.WriteLine("Contraseña Incorrecta.");
                    Console.ReadKey();
                    Console.Clear();
                }

            }


            //****************Fununcion para inicio de session******************





            //Funcion para verificar que el String no este vacio

            static string VerificacionString()
            {
                string Info = "";

                while (Info == "")
                {
                    Info = Console.ReadLine();
                    if (Info == "")
                    {
                        Console.Write("Favor de no dejar en blanco:  ");
                    }
                }

                return Info;
            }

            //Funcion para Ocultar Contraseñas
            static string OcultarContra()
            {
                var pwd = "";
                while (true)
                {
                    while (true)
                    {
                        ConsoleKeyInfo i = Console.ReadKey(true);
                        if (i.Key == ConsoleKey.Enter)
                        {
                            break;
                        }
                        else if (i.Key == ConsoleKey.Backspace)
                        {
                            if (pwd.Length > 0)
                            {
                                pwd.Remove(pwd.Length - 1, 1);
                                Console.Write("\b \b");
                            }
                        }
                        else if (i.KeyChar != '\u0000') // KeyChar == '\u0000' if the key pressed does not correspond to a printable character, e.g. F1, Pause-Break, etc
                        {
                            pwd += i.KeyChar;
                            Console.Write("*");
                        }

                    }
                    if (pwd == "")
                    {
                        Console.Clear();
                        Console.WriteLine("\n Por favor introducir un valor: ");
                    }

                    else
                    {
                        Console.Clear();

                        break;
                    }

                }

                return pwd;
            }



            //Bucle principal del programa que correra mientras la Opcion no sea 4
            do
            {
                Opcion = MenuPrincipal(); //Funcion que imprime el menu, y guarda la Opcion elegida.

                //Metodo Switch para validar la Opcion seleccionada
                switch (Opcion)
                {
                    //Opcion #1 - Crear usuario
                    case 1:

                        InputAgregarUsuario(Contra, NombreUsuario, ApellidoUsuario, ContraUsuario, CorreoUsuario, conexion, query, id, comando);

                        break;

                    case 2:
                        InputModificarUsuario(Contra, NombreUsuario, ApellidoUsuario, ContraUsuario, CorreoUsuario, conexion, query, comando, lector, idSeleccionado);

                        break;

                    case 3:

                        Console.Clear();

                        //Solicitar el Correo y la Contraseña para validar que sean exactos.
                        Console.WriteLine("Para ingresar al sistema debe primero Iniciar sesion");
                        Console.Write("Correo: ");
                        IniciarCorreo = VerificacionString();

                        Console.Write("\nContraseña: ");
                        IniciarContra = OcultarContra();

                        try
                        {
                            //Query para validar que el usuario ingresado sea correcto.
                            query = $"SELECT id, Correo, Contraseña FROM usuario WHERE Correo = '{IniciarCorreo}' AND Contraseña = '{IniciarContra}';";
                            comando = new SqlCommand(query, conexion);

                            conexion.Open();

                            lector = comando.ExecuteReader();

                            while (lector.Read())
                            {
                                idDB = Convert.ToInt32(lector["id"]);
                                CorreoDB = lector["Correo"].ToString();
                                ContraDB = lector["Contraseña"].ToString();
                            }

                            lector.Close();

                            //Iniciar sesion con el usuario indicado
                            if (IniciarCorreo == CorreoDB && IniciarContra == ContraDB)
                            {
                                Console.Clear();

                                do
                                {
                                    Opcion2 = MenuSecundario(CorreoDB); //Funcion para imprimir el menu secundario

                                    //switch interno de la Opcion 3
                                    switch (Opcion2)
                                    {
                                        //Opcion 1 enviar un Correo.
                                        case 1:
                                            Console.Clear();
                                            UsariosDisponibles(query, comando, conexion, lector); //Metodo para imprimir los usuarios disponibles.

                                            Console.Write("Indique el ID del Correo que desea: ");

                                            try
                                            {
                                                idParaEnviar = Convert.ToInt32(Console.ReadLine());
                                            }
                                            catch (Exception e)
                                            {
                                                Console.Clear();
                                                Console.WriteLine($"Se produjo un error de tipo: {e.GetType().ToString()}");
                                                Console.ReadKey();
                                                Console.Clear();
                                                break;
                                            }

                                            //Escribir el Asunto
                                            Console.Write("Asunto: ");
                                            AsuntoCorreo = Console.ReadLine();

                                            //Escribir el Mensaje 
                                            Console.WriteLine("Mensaje:");
                                            MensajeCorreo = Console.ReadLine();

                                            EnviarCorreo(query, idDB, idParaEnviar, AsuntoCorreo, MensajeCorreo, comando, conexion);

                                            break;

                                        //Opcion dos para mostrar la lista de Correos recibidos.
                                        case 2:
                                            MostrarListaCorreos(query, idDB, comando, conexion, lector);

                                            Console.Write("\nIndique el ID del Correo que desea Leer o precione 0 para salir: ");

                                            try
                                            {
                                                LeerCorreo = Convert.ToInt32(Console.ReadLine());
                                            }
                                            catch (Exception e)
                                            {
                                                Console.Clear();
                                                Console.WriteLine($"Se produjo un error de tipo: {e.GetType().ToString()}");
                                                Console.ReadKey();
                                                Console.Clear();
                                                break;
                                            }

                                            Console.Clear();

                                            if (LeerCorreo != 0)
                                            {
                                                //Mostrar el Correo seleccionado
                                                MostrarCorreoSeleccionado(query, LeerCorreo, comando, conexion, lector);
                                                Console.Write("Precione cualquier tecla para salir");
                                                Console.ReadKey();
                                                Console.Clear();

                                            }
                                            else
                                            {
                                                Console.WriteLine("Se regresado al menu del Correo");
                                                Console.ReadKey();
                                                Console.Clear();
                                            }

                                            break;

                                        //Cuando se selecciona la Opcion 3 que es cerrar sesion.
                                        case 3:
                                            Console.Clear();
                                            Console.WriteLine("Se cerrara la sesion de su Correo");
                                            Console.ReadKey();
                                            Console.Clear();
                                            break;
                                    }
                                } while (Opcion2 != 3);

                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("Correo o Contraseña son incorrectos.");
                                Console.ReadKey();
                            }

                        }
                        catch (Exception e)
                        {
                            Console.Clear();
                            Console.WriteLine($"Se produjo un error de tipo: {e.GetType().ToString()}");
                            Console.ReadKey();
                            Console.Clear();

                        }
                        finally
                        {
                            lector.Close();
                            conexion.Close(); //Cerrar la base de datos sin importar si se registro el usaurio o si hubo un error.
                        }


                        break;
                }


            } while (Opcion != 4);
            Console.Clear();
            Console.WriteLine("Tenga un buen dia!");
        }

        //Funcion para imprimir el Menu Principal y caputrar la Opcion del usuario.
        static int MenuPrincipal()
        {
            int Opcion = 0; //variable Opcion para capturar eleccion

            //Impresion de la lista de Menu

            Console.WriteLine("|*********(Bienvenidos al sistema de Correos del Grupo 7.)***********|");
            Console.WriteLine("|*                                                                  *|");
            Console.WriteLine("|*  1.-Crear un usuario.(Requiere Contraseña de admin.)             *|");
            Console.WriteLine("|*  2.-Modificar un usuario.(Requiere Contraseña de adm.            *|");
            Console.WriteLine("|*  3.-Iniciar Sesion.                                              *|");
            Console.WriteLine("|*  4.-Cerrar el programa.                                          *|");
            Console.WriteLine("|*                                                                  *|");
            Console.WriteLine("|********************************************************************|");
            Console.Write("\n");
            Console.Write("Por favor indique el numero de la Opcion que desea: ");



            //try catch
            try
            {
                Opcion = Convert.ToInt32(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine($"Se produjo un error de tipo: {e.GetType().ToString()}");
                Console.ReadKey();
                Console.Clear();
            }

            return Opcion;
        }

        //Metodo para ingresar un usuario a la base de datos.
        static void IngresarUsuario(string query, string NombreUsuario, string ApellidoUsuario, string CorreoUsuario, string ContraUsuario, SqlCommand comando, SqlConnection conexion, int id)
        {
            try
            {
                //Query que ingresa los datos a la base de datos
                query = $"INSERT INTO dbo.usuario(Nombre, Apellido, Correo, Contraseña) OUTPUT INSERTED.id VALUES('{NombreUsuario}', '{ApellidoUsuario}', '{CorreoUsuario}', '{ContraUsuario}')";

                comando = new SqlCommand(query, conexion);
                conexion.Open();
                id = (int)comando.ExecuteScalar();
                Console.WriteLine("\nUsuario Registrado Exitosamente!!");
                conexion.Close();
                Console.ReadKey();
                Console.Clear();

            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine($"Se produjo un error de tipo: {e.GetType().ToString()}");
                Console.ReadKey();
                Console.Clear();

            }
            finally
            {
                conexion.Close(); //Cerrar la base de datos sin importar si se registro el usaurio o si hubo un error.
            }
        }

        //Metodo para mostrar los usuarios disponibles en la base de datos.
        static void UsariosDisponibles(string query, SqlCommand comando, SqlConnection conexion, SqlDataReader lector)
        {
            try
            {
                query = "SELECT id, Correo FROM usuario";
                comando = new SqlCommand(query, conexion);

                conexion.Open();

                lector = comando.ExecuteReader();
                Console.WriteLine("===============================");

                while (lector.Read())
                {
                    Console.WriteLine($"ID: {lector["id"].ToString()} | Correo: {lector["Correo"].ToString()}\n");

                }

                lector.Close();

            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine($"Se produjo un error de tipo: {e.GetType().ToString()}");
                Console.ReadKey();
                Console.Clear();

            }
            finally
            {
                lector.Close();
                conexion.Close(); //Cerrar la base de datos sin importar si se registro el usaurio o si hubo un error.
            }
        }

        //Metodo para modificar un usuario en especifico. 
        static void ModificarUsario(string query, string NombreUsuario, string ApellidoUsuario, string CorreoUsuario, string ContraUsuario, int idSeleccionado, SqlCommand comando, SqlConnection conexion)
        {
            try
            {
                //Query que ingresa los datos a la base de datos
                query = $"update dbo.usuario set Nombre = '{NombreUsuario}', Apellido = '{ApellidoUsuario}', Correo = '{CorreoUsuario}', Contraseña = '{ContraUsuario}' where id = {idSeleccionado};";

                comando = new SqlCommand(query, conexion);
                conexion.Open();
                comando.ExecuteScalar();
                Console.WriteLine("\nUsuario Modificado");
                conexion.Close();
                Console.ReadKey();
                Console.Clear();

            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine($"Se produjo un error de tipo: {e.GetType().ToString()}");
                Console.ReadKey();
                Console.Clear();

            }
            finally
            {
                conexion.Close(); //Cerrar la base de datos sin importar si se registro el usaurio o si hubo un error.
            }
        }

        //Funcion que imprime el menu secundario
        static int MenuSecundario(string CorreoDB)
        {
            int Opcion = 0; //variable Opcion para capturar eleccion

            //Impresion de la lista de Menu

            Console.WriteLine($"|****(Bienvenidos a su buzon de Correo: { CorreoDB}*******|");
            Console.WriteLine("|*                                                        *|");
            Console.WriteLine("|*           1.-Enviar Correo                             *|");
            Console.WriteLine("|*           2.-Mostrar Correos Recibidos                 *|");
            Console.WriteLine("|*           3.-Salir                                     *|");
            Console.WriteLine("|*                                                        *|");
            Console.WriteLine("|*                                                        *|");
            Console.WriteLine("|**********************************************************|");
            Console.Write("\n");
            Console.Write("Por favor indique el numero de la Opcion que desea: ");



            //try catch
            try
            {
                Opcion = Convert.ToInt32(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine($"Se produjo un error de tipo: {e.GetType().ToString()}");
                Console.ReadKey();
                Console.Clear();
            }

            return Opcion;
        }

        //Metodo para enviar un Correo
        static void EnviarCorreo(string query, int idDB, int idParaEnviar, string AsuntoCorreo, string MensajeCorreo, SqlCommand comando, SqlConnection conexion)
        {
            try
            {
                Console.Clear();
                //Query que ingresa los datos a la base de datos
                query = $"INSERT INTO dbo.Correo(remitenteid, destinoid, Asunto, Mensaje) VALUES({idDB}, {idParaEnviar}, '{AsuntoCorreo}', '{MensajeCorreo}')";

                comando = new SqlCommand(query, conexion);
                conexion.Open();
                comando.ExecuteScalar();
                Console.WriteLine("\nCorreo enviado.");
                conexion.Close();
                Console.ReadKey();
                Console.Clear();

            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine($"Se produjo un error de tipo: {e.GetType().ToString()}");
                Console.ReadKey();
                Console.Clear();

            }
            finally
            {
                conexion.Close(); //Cerrar la base de datos sin importar si se envio el Correo o si hubo un error.
            }
        }

        //Metodo para mostrar la lista de Correos de dicho usuario.
        static void MostrarListaCorreos(string query, int idDB, SqlCommand comando, SqlConnection conexion, SqlDataReader lector)
        {
            try
            {
                query = $"select c.id, Correo as 'Remitente', Asunto as 'Asunto' from usuario as u, Correo as c where destinoid= {idDB} and u.id = remitenteid;";
                comando = new SqlCommand(query, conexion);

                conexion.Open();

                lector = comando.ExecuteReader();

                Console.Clear();

                Console.WriteLine("===============================");

                while (lector.Read())
                {
                    Console.WriteLine($"ID: {lector["id"].ToString()} | Asunto: {lector["Asunto"].ToString()}\n");

                }

                lector.Close();

            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine($"Se produjo un error de tipo: {e.GetType().ToString()}");
                Console.ReadKey();
                Console.Clear();

            }
            finally
            {
                lector.Close();
                conexion.Close(); //Cerrar la base de datos sin importar si se registro el usaurio o si hubo un error.
            }
        }

        //Mostrar el Correo seleccionado.
        static void MostrarCorreoSeleccionado(string query, int LeerCorreo, SqlCommand comando, SqlConnection conexion, SqlDataReader lector)
        {
            try
            {
                query = $"select Asunto as 'Asunto', Mensaje as 'Mensaje' from Correo as c where id = {LeerCorreo};";
                comando = new SqlCommand(query, conexion);

                conexion.Open();

                lector = comando.ExecuteReader();

                Console.Clear();

                Console.WriteLine("===============================");

                while (lector.Read())
                {
                    Console.WriteLine($"Asunto: {lector["Asunto"].ToString()} \nMensaje: {lector["Mensaje"].ToString()}\n");

                }

                lector.Close();

            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine($"Se produjo un error de tipo: {e.GetType().ToString()}");
                Console.ReadKey();
                Console.Clear();

            }
            finally
            {
                lector.Close();
                conexion.Close(); //Cerrar la base de datos sin importar si se registro el usaurio o si hubo un error.
            }
        }
    }
}
